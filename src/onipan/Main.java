package onipan;

/**鬼のパンツの歌を表示
 * @author 200217AM
 *
 */
public class Main {

	public static void main(String[] args) {
		execute();
	}

	/**改行を行う
	 * @return
	 */
	public static String newLine(){
		return "\n";
	}

	/**同じメッセージを複数回表示する
	 *
	 */
	public static String repeatMessage(String msg,int count){
		String repeatMsg="";
		for(int i=0; i<count;i++){
			repeatMsg+=msg;
		}
		return repeatMsg;
	}

	/**鬼のパンツはいいパンツを表示する
	 * @return
	 */
	public static String sayOnipanIsGood(){
		return "鬼のパンツはいいパンツ";
	}

	/**強いぞを表示する
	 * @return
	 */
	public static String sayStrong(){
		return "強いぞ";
	}

	/**鬼の毛皮でできているを表示
	 *
	 */
	public static String sayMadeOfOniFar(){
		return "鬼の毛皮でできている";
	}

	/**5年はいてもやぶれないを表示
	 * @return
	 */
	public static String sayFiveYearsSafe(){
		return "5年はいてもやぶれない";
	}

	/**10年はいてもやぶれないを表示
	 * @return
	 */
	public static String sayTenYearsSafe(){
		return "10年はいてもやぶれない";
	}

	/**はこうを表示
	 * @return
	 */
	public static String sayLetUsWear(){
		return "はこう";
	}

	/**鬼のパンツを表示
	 * @return
	 */
	public static String sayOnipan(){
		return "鬼のパンツ";
	}

	/**あなたもを表示
	 * @return
	 */
	public static String sayYouToo(){
		return "あなたも";
	}

	/**みんなでを表示
	 * @return
	 */
	public static String sayWithEveryone(){
		return "みんなで";
	}

	public static void execute(){
		String lyric="";
		lyric+=sayOnipanIsGood();
		lyric+=newLine();
		lyric+=repeatMessage(sayStrong(),2);
		lyric+=newLine();
		lyric+=sayMadeOfOniFar();
		lyric+=newLine();
		lyric+=repeatMessage(sayStrong(), 2);
		lyric+=newLine();
		lyric+=sayFiveYearsSafe();
		lyric+=newLine();
		lyric+=repeatMessage(sayStrong(), 2);
		lyric+=newLine();
		lyric+=sayTenYearsSafe();
		lyric+=newLine();
		lyric+=repeatMessage(sayStrong(), 2);
		lyric+=newLine();
		lyric+=repeatMessage(sayLetUsWear(), 2);
		lyric+=sayOnipan();
		lyric+=newLine();
		lyric+=repeatMessage(sayLetUsWear(), 2);
		lyric+=sayOnipan();
		lyric+=newLine();
		lyric+=repeatMessage(sayYouToo(), 4);
		lyric+=newLine();
		lyric+=sayWithEveryone();
		lyric+=sayLetUsWear();
		lyric+=sayOnipan();
		System.out.println(lyric);
	}

}
